import System.IO (hClose, openFile, IOMode(ReadMode))
import Data.List (foldl')

main :: IO ()
main = do
    d3input <- openFile "d3input.txt" ReadMode
    raw <- getContents
    hClose d3input
    let input = lines raw :: [String]
        
        -- Logistical calculations to prepare for the actual solution
        lineCount = length input :: Int
        lineSize = length $ head input :: Int

        -- This is where the actual calculations start
        take1Drop1 = defSlopeRoute input lineCount lineSize 1 1 1
        take3Drop1 = defSlopeRoute input lineCount lineSize 1 3 1
        take5Drop1 = defSlopeRoute input lineCount lineSize 1 5 1
        take7Drop1 = defSlopeRoute input lineCount lineSize 1 7 1
        take1Drop2 = defSlopeRoute input lineCount lineSize 1 1 2
        partTwoProduct = [take1Drop1, take3Drop1, take5Drop1, take7Drop1, take1Drop2]
    print $ countTrees take3Drop1  -- Answer to part 1
    print . foldl' (*) 1 $ map countTrees partTwoProduct  -- Answer to part 2

lineRepFactor :: Int -> Int -> Int -> Int
lineRepFactor takeParam lineCount lineSize
    = (lineCount * takeParam) `div` lineSize + 1

expandInput :: [String] -> Int -> [String]
expandInput input lineReplicationFactor
    = map (mconcat . replicate lineReplicationFactor) input :: [String]

slopeRoute :: [String] -> Int -> Int -> Int -> [String]
slopeRoute [] _ _ _ = []
slopeRoute expandedInput accumTake takeParam 1
    = take accumTake (head expandedInput)
    : slopeRoute (tail expandedInput) (accumTake + takeParam) takeParam 1
slopeRoute expandedInput accumTake takeParam stepParam
    = take accumTake (head expandedInput)
    : slopeRoute (drop stepParam expandedInput) (accumTake + takeParam) takeParam stepParam

defSlopeRoute :: [String] -> Int -> Int -> Int -> Int -> Int -> [String]
defSlopeRoute input lineCount lineSize accumTake takeParam stepParam
    = slopeRoute expandedInput accumTake takeParam stepParam
  where
    toReplicate = lineRepFactor takeParam lineCount lineSize
    expandedInput = expandInput input toReplicate

countTrees :: [String] -> Int
countTrees input = length . filter (== '#') $ map last input