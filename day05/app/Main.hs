module Main where

import Data.List ((\\))

main :: IO ()
main = do
    raw <- readFile "./data/day05Input.txt"

    let input = lines raw :: [String]
        rows = [0..127]
        cols = [0..7]
        parsed = map (toRowsAndCols rows cols) input :: [(Int, Int)]
        allSeats = [(x, y) | x <- [0..127], y <- [0..7]] :: [(Int, Int)]

    print . maximum $ map (\(x, y) -> x * 8 + y) parsed  -- Part 1
    print $ allSeats \\ parsed  -- Part 2 (solved via visual inspection)

indexIdentifier :: String -> [Int] -> Int
indexIdentifier (x:xs) refList
    | length (x:xs) == 1 && x `elem` ['F', 'L'] = head refList
    | length (x:xs) == 1 && x `elem` ['B', 'R'] = last refList
    | x `elem` ['F', 'L'] = indexIdentifier xs (take (length refList `div` 2) refList)
    | otherwise           = indexIdentifier xs (drop (length refList `div` 2) refList)

toRowsAndCols :: [Int] -> [Int] -> String -> (Int, Int)
toRowsAndCols rowRef colRef x = (indexIdentifier (take 7 x) rowRef, indexIdentifier (drop 7 x) colRef)