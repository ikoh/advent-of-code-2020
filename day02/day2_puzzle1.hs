main :: IO Integer
main = do
    contents <- readFile "day2_input.txt"
    return $ sum . map flagValidPasswords $ lines contents

-- main :: IO ()
-- main = interact $ show . sum . map flagValidPasswords . lines  readFile "day2_input.txt"

-- Sanitise individual password/policy strings
replaceHyphens :: Char -> Char
replaceHyphens strElem
    | strElem == '-' = ' '
    | otherwise = strElem

makeUsable :: [Char] -> [String]
makeUsable rawInput = words (filter (/= ':') (map replaceHyphens rawInput))

-- Verify passwords and identify valid ones
countKeyChar :: Eq a => [[a]] -> Int
countKeyChar parsedList = length $ filter (== keyChar) password
    where keyChar = head (parsedList !! 2)
          password = last parsedList

policyChecker :: Num p => [String] -> p
policyChecker parsedList
    | head validityRange <= countedChars && countedChars <= last validityRange = 1
    | otherwise = 0
    where countedChars = countKeyChar parsedList
          validityRange = map read $ take 2 parsedList :: [Int]

flagValidPasswords :: [Char] -> Integer
flagValidPasswords = policyChecker . makeUsable
