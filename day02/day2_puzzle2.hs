main :: IO Int
main = do
    contents <- readFile "day2_input.txt"
    return $ sum . map newFlagValidPasswords $ lines contents

-- Sanitise individual password/policy strings (reuse from puzzle 1)
replaceHyphens :: Char -> Char
replaceHyphens strElem
    | strElem == '-' = ' '
    | otherwise = strElem

makeUsable :: [Char] -> [String]
makeUsable rawInput = words (filter (/= ':') (map replaceHyphens rawInput))

-- Check validity of password given updated password policy
isValidPassword :: [[Char]] -> Int
isValidPassword parsedList
    | (length $ filter (== keyChar) [password !! (pos1 - 1), password !! (pos2 - 1)]) == 1 = 1
    | otherwise = 0 
    where password = last parsedList
          pos1 = read $ head parsedList :: Int
          pos2 = read $ parsedList !! 1 :: Int 
          keyChar = head $ parsedList !! 2

newFlagValidPasswords :: [Char] -> Int
newFlagValidPasswords = isValidPassword . makeUsable
