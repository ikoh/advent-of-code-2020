_Last updated on 2021-11-25_

* Use `readFile` instead of `openFile`
* The path given to `readFile` is relative to the directory level where I run `cabal run` and not that of `Main.hs`
* I removed `cid` at the `parsedInput` step, which worked nicely for part 1 but will most likely bite me in part 2
