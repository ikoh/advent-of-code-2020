module Main where

import Data.List.Split (splitOn)
import Data.List       (isSuffixOf, isPrefixOf)

main :: IO ()
main = do
    raw <- readFile "./data/day4Input.txt"

    let input = lines raw :: [String]

        -- Because the input records are presented inconsistently, inputAllLines
        -- makes it so that every key-value pair occupies its own line.
        inputAllLines
            = lines
            . unlines
            $ map (map space2Newline) input
            :: [String]

        inputParsed
            = filter (\x -> length x == 7)
            . map (filter (\x -> "cid" `isPrefixOf` x == False))
            $ splitOn [""] inputAllLines
            :: [[String]]

        validPassportsPart2
            = length
            . filter (== 0)
            $ map (sum . map (custFieldValidator . str2Tup)) inputParsed
            :: Int

    print $ length inputParsed  -- Part 1
    print validPassportsPart2  -- Part 2

space2Newline :: Char -> Char
space2Newline ' ' = '\n'
space2Newline x   = x

str2Tup :: String -> (String, String)
str2Tup x = (head toList, last toList)
  where
    toList = splitOn ":" x

str2Int :: String -> Int
str2Int x = read x :: Int

-- To use for the byr, iyr and eyr fields
yearValidator :: String -> Int -> Int -> Int
yearValidator x lb ub
    | xInt >= lb && xInt <= ub = 0
    | otherwise = 1
  where
    xInt = str2Int x

-- Use as helper function while validating hcl and pid
charValidator :: String -> Char -> Int
charValidator reference x
    | x `elem` reference = 0
    | otherwise = 1

hclValidator :: String -> Int
hclValidator x
    | take 1 x == "#" && sum digitValidator == 0  && length (drop 1 x) == 6 = 0
    | otherwise = 1
  where
    reference = mconcat [['a'..'f'], ['0'..'9']]
    digitValidator = map (charValidator reference) (drop 1 x)

eclValidator :: String -> Int
eclValidator x
    | x `elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] = 0
    | otherwise = 1

pidValidator :: String -> Int
pidValidator x
    | length x == 9 && sum digitValidator == 0 = 0
    | otherwise = 1
  where
    reference = ['0'..'9']
    digitValidator = map (charValidator reference) x

getHeightNum :: String -> Int
getHeightNum x = str2Int $ take (length x - 2) x

hgtValidator :: String -> Int
hgtValidator x
    | "cm" `isSuffixOf` x && getHeightNum x >= 150 && getHeightNum x <= 193 = 0
    | "in" `isSuffixOf` x && getHeightNum x >= 59 && getHeightNum x <= 76   = 0
    | otherwise                                                             = 1

custFieldValidator :: (String, String) -> Int
custFieldValidator ("byr", x) = yearValidator x 1920 2002
custFieldValidator ("iyr", x) = yearValidator x 2010 2020
custFieldValidator ("eyr", x) = yearValidator x 2020 2030
custFieldValidator ("hgt", x) = hgtValidator x
custFieldValidator ("hcl", x) = hclValidator x
custFieldValidator ("ecl", x) = eclValidator x
custFieldValidator ("pid", x) = pidValidator x